FROM jindraivanek/fable-azure-build
SHELL ["/bin/bash", "-c"]

WORKDIR /build

ARG AZ_USER
ENV AZ_USER=${AZ_USER}
ARG AZ_PASS
ENV AZ_PASS=${AZ_PASS}
ARG AZ_TENANT
ENV AZ_TENANT=${AZ_TENANT}

# Package lock files are copied independently and their respective package
# manager are executed after.
#
# This is voluntary as docker will cache images and only re-create them if
# the already-copied files have changed, by doing that as long as no package
# is installed or updated we keep the cached container and don't need to
# re-download.

# Initialize node_modules
COPY package.json yarn.lock ./
RUN yarn install

# Initialize paket packages
COPY paket.dependencies paket.lock ./
COPY .paket .paket
RUN paket restore

# Copy everything else and run the build
COPY . ./
RUN rm -rf deploy
RUN fake run build.fsx --target Bundle

RUN cp -r deploy/Client/public public

RUN if [ "x$AZ_USER" != "x" ] && [ "x$AZ_PASS" != "x" ] && [ "x$AZ_TENANT" != "x" ]; then az login --service-principal --username $AZ_USER --password $AZ_PASS --tenant $AZ_TENANT; fi
RUN if [ "x$AZ_USER" != "x" ] && [ "x$AZ_PASS" != "x" ] && [ "x$AZ_TENANT" != "x" ]; then fake run build.fsx --target DeployServer; fi

# FROM microsoft/dotnet:2.1-aspnetcore-runtime-alpine
# WORKDIR /app
# COPY --from=builder /build/deploy ./
# WORKDIR /app/Server
# EXPOSE 8085
# ENTRYPOINT ["dotnet", "Server.dll"]
