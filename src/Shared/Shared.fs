namespace Shared

module Map =
    let merge f m1 m2 =
        let keys = Seq.append (m1 |> Map.toSeq) (m2 |> Map.toSeq) |> Seq.map fst |> Seq.distinct
        keys |> Seq.choose (fun k -> f (Map.tryFind k m1) (Map.tryFind k m2) |> Option.map (fun x -> k, x))
        |> Map.ofSeq
module Const =
    let sourceSizeLimit = 100 * 1024

type FantomasOption =
    | BoolOption of bool
    | IntOption of int
module FantomasOption =
    let toString = function
        | BoolOption x -> sprintf "%b" x
        | IntOption x -> sprintf "%i" x

type FantomasOptions = (string * FantomasOption) []
module FantomasOptions =
    open Fable.SimpleJson
    let toJson m =
        m |> Seq.map (function | k, BoolOption x -> k, JBool x | k, IntOption x -> k, JNumber (float x))
        |> Map.ofSeq |> JObject |> SimpleJson.toString
    let ofJson json =
        match json |> SimpleJson.tryParse with
        | Some (JObject xs) -> 
            try
                xs |> Map.map (fun _ v -> 
                    match v with 
                    | JBool x -> BoolOption x 
                    | JNumber x -> IntOption (int x)
                    | _ -> failwith "")
                |> Map.toArray |> Some
            with _ -> None            
        | _ -> None

type EditorState =
    | Loading
    | Loaded of string
    | FormatError of string
type Model =
    { Source : string
      DefaultConfig : FantomasOptions
      Config : FantomasOptions
      Version : string
      FSharpEditorState : EditorState
      BackendName : string }
    member __.IsLoading =
        __.FSharpEditorState = Loading
    member __.IsError =
        match __.FSharpEditorState with
        | FormatError _ -> true
        | _ -> false
    member __.Formatted =
        match __.FSharpEditorState with
        | Loading -> ""
        | Loaded code -> code
        | FormatError err -> err
    member m.GetConfig =
        let mergeF x y = 
            match x, y with 
            | None, Some x 
            | Some x, _ -> Some x 
            | _ -> None
        Map.merge mergeF (Map.ofArray m.Config) (Map.ofArray m.DefaultConfig) |> Map.toArray
    static member Default =
                            { Source = ""
                              DefaultConfig = Array.empty
                              Config = Array.empty
                              Version = ""
                              FSharpEditorState = Loading
                              BackendName = "current" }

module Model =
    let configDiffFromDefault config defaultConfig =
        let mergeF x y = 
            match x, y with 
            | Some x, Some y -> if x = y then None else Some x 
            | _ -> None
        let newConfig = Map.merge mergeF (Map.ofArray config) (Map.ofArray defaultConfig) |> Map.toArray
        newConfig

module Route =
    /// Defines how routes are generated on server and mapped from client
    let builder typeName methodName =
        sprintf "/api/%s/%s" typeName methodName
    
/// A type that specifies the communication protocol between client and server
/// to learn more, read the docs at https://zaid-ajaj.github.io/Fable.Remoting/src/basics.html
type IModelApi =
    {
        init : unit -> Async<Model>
        version : unit -> Async<string>
        format : string -> FantomasOptions -> Async<Result<string, string>>
        options : unit -> Async<FantomasOptions>
    }

module Reflection =
    open FSharp.Reflection
    let inline getRecordFields x =
        let names = FSharpType.GetRecordFields (x.GetType()) |> Seq.map (fun x -> x.Name)
        let values = FSharpValue.GetRecordFields x
        let data = Seq.zip names values |> Seq.toArray
        data