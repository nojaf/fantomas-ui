module Config

type AzureFunctionsDef = {
    Name : string
    DisplayName : string
    AzureFunctionsName :string
    AzureFunctionsUrl : string }

let azureFunctions = [
    {  Name = "current"
       DisplayName = "Current"
       AzureFunctionsName = "fantomas"
       AzureFunctionsUrl = "https://fantomas.azurewebsites.net" }
    {  Name = "preview"
       DisplayName = "Preview"
       AzureFunctionsName = "fantomas-preview"
       AzureFunctionsUrl = "https://fantomas-preview.azurewebsites.net" }
    {  Name = "v2-9-2"
       DisplayName = "Version 2.9.2"
       AzureFunctionsName = "fantomas-2-9-2"
       AzureFunctionsUrl = "https://fantomas-2-9-2.azurewebsites.net" }
    ]

let azureFunctionsRedirect = [ "preview-trivia", "preview" ] |> Map.ofList
let azureFunctionsApplyRedirect x = azureFunctionsRedirect |> Map.tryFind x |> Option.defaultValue x

let azureFunctionsName = azureFunctions |> Seq.head |> fun x -> x.AzureFunctionsName
let azureFunctionsUrl = azureFunctions |> Seq.head |> fun x -> x.AzureFunctionsName

let getBackendUrl name =
    azureFunctions |> Seq.tryFind (fun c -> c.Name = name) |> Option.map (fun c -> c.AzureFunctionsUrl)
    |> Option.defaultValue azureFunctionsUrl
